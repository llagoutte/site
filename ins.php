<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>

<body>

    <div id="container">
        <div id="header">
            <h1><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Inscription</h1>
        </div>
        <form method="post" action="ins2.php">
            <input class="inputText" type="text" name="pseudo" placeholder="Pseudo"><br /><br />
            <input class="inputText" type="password" name="mdp" placeholder="Mot De Passe"><br /><br />
            <input class="inputText" type="text" name="clef" placeholder="Clé de sécurité"><br /><br /><br/>
            <input class="inputSubmit" type="submit" name="" value="S'inscrire">
        </form>
        <div id="footer">
            <a href="connexion.php"><i class="fa fa-user-plus" aria-hidden="true"></i>  Retour</a>
        </div>
    </div>
</body>
</html>