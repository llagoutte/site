<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
	<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>
<p id="centreTexte" class="w100 rouge">Si tu as accès ici sâche que tu est priviligié !</p>
<form method="post" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="52428800" />
	<input type="file" name="image" id="image"/>
	<input type="submit" value="Valider">
<?php

$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );

if ( in_array($extension_upload,$extensions_valides) ) echo "Extension correcte";

$nom = "/zone404/";

$resultat = move_uploaded_file($_FILES['image']['tmp_name'],$nom);
if ($resultat) echo "Transfert réussi";

?>
</form>
</body>
</html>