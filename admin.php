<?php
session_start();

	
	if(empty($_SESSION['pseudo']) OR $_SESSION['groupe'] != 'Admin' OR $_SESSION['niveau'] < 4){	

		header('HTTP/1.1 404 Not Found');
		header('Location: connexion.php');
		exit;
	}

require 'bdd.php';

?>
<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
	<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>


	<div id="container">
		<div id="header">
            <h1><i class="fa fa-key" aria-hidden="true"></i>  Création/Suppression de clefs</h1>
        </div>
	    <p id="texteCentre" class="rouge">Ajout d'une nouvelle clef :</p>

		<form method="post">
		<input placeholder="Indiquer la clef" class="inputText" type="text" name="clef"/><br/><br/>
		<input placeholder="Nombre de validité" class="inputText" type="text" name="valide"/><br/><br/>
		<input class="inputSubmit" type="submit" value="Valider"/>
		</form>

		<p id="texteCentre" class="rouge">Supprimer une clef :</p>
		<form method="post">
			<input class="inputText" type="text" name="vclef" placeholder="Nom de la clé"/><br/><br/>
			<input class="inputSubmit" type="submit" value="Supprimer">
		</form>
		<br/>
		<p id="texteCentre" class="rouge">Assigner un groupe à un utilisateur :</p>
		<form method="post">
			<input class="inputText" type="text" name="pseudo" placeholder="Pseudo" /><br/><br/>
			<select id="groupelist" name="groupe" class="inpuText">
			    <option value="Normal">Normal</option>
    			<option value="Prof">Prof</option>
    			<option value="BTS-1">BTS-1</option>
    			<option value="VIP">VIP</option>
    			<option value="Admin">Admin</option>
  			</select>
  			<br/><br/>
			<input class="inputSubmit" type="Submit" value="Assigner">
		</form>

	





<?php
	
	$valide = $_POST['valide'];
	$clef = $_POST['clef'];

/*Verification si une clef peut être crée*/

if(!empty($_POST["valide"]) OR !empty($_POST["clef"])){

    $inser = $bdd->prepare('INSERT INTO vclef(clef, validite) VALUES(:clef, :validite)');
    $inser->execute(array(
    'clef' => $clef,
    'validite' => $valide
    ));

    echo "\n" . '<p class="w100 rouge" id="texteCentre">La clef ' . $clef . ' à été ajouté avec succès.</p></br>';
}

	

	/*Liste des clef active avec la le nombre d'utilisations possibles*/
	$lsclef = $bdd->query('SELECT * FROM vclef ORDER BY validite');
	
	?>

	<br/>

	<?php
    		
    		while ($listeclef = $lsclef->fetch())
				{
					echo '<p id="texteCentre">La clé <span id="gras">' . $listeclef['clef'] . '</span> est valide encore <span id="gras">' . $listeclef['validite'] . '</span> fois</p><br/>';
				}
	
				
				$lsclef->closeCursor();

			/*Outil de suppression des clés*/
			if(!empty($_POST['vclef']))
			{
				$vclef = $_POST['vclef'];
				$suppr = $bdd->prepare('DELETE FROM vclef WHERE clef = ?');
    			$suppr->execute(array($vclef));
			}

	if(!empty($_POST['pseudo']) AND !empty($_POST['groupe'])){

	$pseudo = $_POST['pseudo'];
	$groupe = $_POST['groupe'];

	if($groupe == 'Admin'){
		$niveau = 5;
	}
	if($groupe == 'VIP'){
		$niveau = 4;
	}
	if($groupe == 'Normal'){
		$niveau = 1;
	}
	if($groupe == 'Prof'){
		$niveau = 2;
	}
	if($groupe == 'BTS-1'){
		$niveau = 3;
	}

$assigne = $bdd->prepare('UPDATE membres SET groupe = :groupe, niveau = :niveau WHERE pseudo = :pseudo');

$assigne->execute(array(
    'groupe' => $groupe,
    'niveau' => $niveau,
    'pseudo' => $pseudo
	));
echo '<p class="w100 rouge" id="texteCentre">Le changement à bien était interprêté</p>';
}


	?>
	    <div id="footer">
            <a href="connexion.php"><i class="fa fa-arrow-left" aria-hidden="true"></i>  Accueil</a>
        </div>
</div>
</body>
</html>