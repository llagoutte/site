<?php session_start();

if (empty($_SESSION['groupe']) AND empty($_SESSION['pseudo'])){
	header('Location: connexion.php');
	exit();
}

require 'bdd.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>

<?php require 'menu.php'; ?>

<div id="chat">
	<h1 id="texteCentre" class="w100 rouge">Chat BTS-1</h1>
	<div id="affichage">
<?php

$message = $bdd->query('SELECT pseudo, contenu, groupe FROM chat ORDER BY ID DESC LIMIT 0, 10');

while ($allmessage = $message->fetch())
{
	if($allmessage['groupe'] == 'Admin'){

	echo '<p class="w100 minichat"><strong class="rouge pseudo">' . htmlspecialchars($allmessage['pseudo']) . '</strong> : ' . htmlspecialchars($allmessage['contenu']) . '</p>';

}
else{	

	echo '<p class="w100"><strong class="pseudo">' . htmlspecialchars($allmessage['pseudo']) . '</strong> : ' . htmlspecialchars($allmessage['contenu']) . '</p>';

}
}

$message->closeCursor();

?>
	</div>
	
	<form method="post" action="chatpost.php">
		<input class="inputText" type="text" name="texte">
		<input class="inputSubmit" type="submit" value="Envoyer">	
	</form>
</div>

</body>
</html>