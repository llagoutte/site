<?php session_start(); 
if (empty($_SESSION['pseudo']) OR empty($_SESSION['groupe'])){ 
 
 	header('Location: connexion.php');
 	exit;

} ?>
<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>

<?php require 'menu.php'; ?>

   	<div id="yt">
   		</br>
   		<center><p id="gras" class="ytt">Vidéo youtube</p></center>

   				<?php 
		if($_SESSION['groupe'] == 'Admin'){ ?>

		<form method="post">
			<input class="inputText" type="text" name="vdyt">
			<input class="inputSubmit" type="submit" value="Valider">
		</form>

		<?php

		$vdyt = $_POST['vdyt'];

		 } ?>

   		</br>
		<iframe width="100%" height="400" src=<?php echo 'https://youtube.com/embed/2bjk26RwjyU'; ?> frameborder="0" allowfullscreen></iframe>

	</div>

	<div id="actu">
		<h1 class="actual w100">Actualité de la classe</h1>
		<p class="w100 actualite">Site en cours de construction, site entiérement réalisée à la main aucune template n'a été utilisé.</p>
	</div>

	<div id="prof">
		<h1 class="actual w100">Espace Prof</h1>
		<p class="w100 actualite">Ici les profs pourront poster toutes les informations qu'ils souhaitent telles que les devoirs, les absences ...</p>
	</div>

</body>
</html>