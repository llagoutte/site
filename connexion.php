<?php 
	session_start();
    if(!empty($_SESSION['pseudo']) AND !empty($_SESSION['groupe'])){
        header('Location: index.php');
        exit;
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Connexion</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>
    <div id="container">
        <div id="header">
            <h1><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>  Connexion</h1>
        </div>
        <form method="post" action="pconnexion.php">
            <input class="inputText" type="text" name="pseudo" placeholder="Pseudo"><br /><br />
            <input class="inputText" type="password" name="mdp" placeholder="Mot De Passe"><br /><br /><br/>
            <input class="inputSubmit" type="submit" name="" value="Se Connecter">
        </form>
        <div id="footer">
            <a href="ins.php"><i class="fa fa-user-plus" aria-hidden="true"></i>  Inscription</a>
        </div>
    </div>
</body>
</html>