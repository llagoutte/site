<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>BTS-1</title>
	<meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
</head>
<body>

<nav>
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="chat.php">Chat</a></li>
		<?php if($_SESSION['groupe'] == 'Admin'){ ?>
		<li><a href="admin.php">Admin</a></li>
	<?php } ?>
	<?php if($_SESSION['niveau'] >= 4){ ?>
		<li><a href="secret.php">Tréfonds</a></li>
	<?php } ?>
		<li><a href="deco.php">Déconnexion</a></li>
	</ul>
</nav>

	<div id="corp">
		<p><div id="butilisateur">Bonjour <span id="gras"><?php echo $_SESSION['pseudo']; ?></span> vous êtes un utilisateur <span id="gras"><?php echo $_SESSION['groupe']; ?></span></div></p>
	</div>

</body>
</html>